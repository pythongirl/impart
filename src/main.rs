mod db;

pub mod snowflake;

mod object;
use object::ObjectId;

// /// A value that can be used for vales of properties of objects
// #[derive(PartialEq, Debug, Clone)]
// enum Value {
//     String(String),
//     Float(f64),
//     Integer(i64),
//     Boolean(bool),
// }

// struct Interface {
//     properties: HashMap<String>,
// }

/// An object usually representing a physical thing in the game world.
///
/// Uniquely identified by it's UUID. The UUID should never be changed.
/// Uses identity equality through it's UUID.
// #[derive(Debug)]
// struct Object {
//     id: Snowflake,
//     name: String,
//     properties: HashMap<String, ()>,
//     contents: HashSet<Arc<Object>>,
// }

// struct Relation {
//     kind: String,
//     subject: Weak<Object>,
//     object: Weak<Object>,
// }

#[tokio::main]
async fn main() {
    // Nowhere is the special root object

    println!("Hello, world!");

    let path = std::path::Path::new("test-env");
    let db = db::create_db(path).expect("could not open database");

    let object_cf = db.cf_handle("objects").expect("could not get objects CF");

    {
        let trans = db.transaction();

        // object_store.clear(writer.as_mut()).unwrap();

        let nowhere = ObjectId::create_object(&object_cf, &trans, "Nowhere")
            .await
            .unwrap();

        let foo = ObjectId::create_object_in(&object_cf, &trans, "Foo", nowhere)
            .await
            .unwrap();

        dbg!(nowhere, foo);

        // ObjectId::remove_object(&object_store, &mut writer, foo, nowhere).unwrap();

        // ObjectId::remove_object(&object_store, &mut writer, nowhere, foo).unwrap();

        // TODO: Should i somehow have commit be a future?
        // idk how i would even do that
        // Yeah idk how :/
        trans.commit().expect("Could not commit transaction");
    }

    db::dump_db(&db, "objects");

    std::mem::drop(db);

    db::DB::destroy(&rocksdb::Options::default(), path).expect("unable to destroy db");
}
