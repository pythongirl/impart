use lazy_static::lazy_static;
use snowdon::ClassicLayout;

pub use snowdon::Error as SnowflakeError;

#[derive(Debug)]
pub struct SnowflakeParams;

impl snowdon::MachineId for SnowflakeParams {
    fn machine_id() -> u64 {
        1
    }
}

impl snowdon::Epoch for SnowflakeParams {
    fn millis_since_unix() -> u64 {
        1704063600000
    }
}

/// The snowflake type used for Impart Objects
pub type Snowflake = snowdon::Snowflake<ClassicLayout<SnowflakeParams>, SnowflakeParams>;

// pub fn snowflake_to_value(flake: Snowflake) -> rkv::Value<'static> {
//     rkv::Value::U64(flake.into_inner())
// }

/// The generator type for Impart Snowflakes
pub type Generator = snowdon::Generator<ClassicLayout<SnowflakeParams>, SnowflakeParams>;

lazy_static! {
    pub(crate) static ref SNOWFLAKE_GENERATOR: Generator = Generator::default();
}

/// Generate a snowflake asynchronously
///
/// Based on [`snowdon::Generator::generate_lock_free`] but will never return
/// the error [`snowdon::Error::SnowflakeExhaustion`] as it will wait and
/// generate a new one.
pub async fn generate_snowflake() -> Result<Snowflake, snowdon::Error> {
    loop {
        match SNOWFLAKE_GENERATOR.generate_lock_free() {
            Ok(s) => break Ok(s),
            Err(snowdon::Error::SnowflakeExhaustion) => {
                tokio::time::sleep(tokio::time::Duration::from_nanos(1)).await;
                continue;
            }
            Err(snowdon::Error::InvalidSnowflake) => {
                unreachable!();
            }
            Err(e) => break Err(e),
        }
    }
}
