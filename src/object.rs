use array_concat::concat_arrays;
use thiserror::Error;

use crate::db;
use crate::snowflake;
use crate::snowflake::Snowflake;

enum ObjectFieldKeyId {
    Name = 1,
    Contents = 2, // Containees? linguistics is fun.
    Container = 3,
    Properties = 4,
}

#[derive(Debug, Error)]
pub enum DBError {
    #[error("Unable to create snowflake {0}")]
    Snowflake(#[from] snowflake::SnowflakeError),
    #[error("storage failure: {0}")]
    DB(#[from] db::Error),
}

#[derive(Debug, Copy, Clone)]
pub struct ObjectId(snowflake::Snowflake);

impl ObjectId {
    /// Generate a new Object ID asynchronously
    async fn new_id() -> Result<ObjectId, snowflake::SnowflakeError> {
        Ok(ObjectId(snowflake::generate_snowflake().await?))
    }

    /// Get an object ID from a raw `u64`
    ///
    /// Returns `None` if the u64 is not a valid Snowflake ID.
    fn from_raw(r: u64) -> Option<ObjectId> {
        Snowflake::from_raw(r).ok().map(ObjectId)
    }

    fn to_bytes(self) -> [u8; 8] {
        self.0.into_inner().to_be_bytes()
    }

    fn key_name(self) -> [u8; 9] {
        concat_arrays!(self.to_bytes(), [ObjectFieldKeyId::Name as u8])
    }

    // fn key_property(self, ) -> SmallVec<[u8; 17]>

    fn key_contents_prefix(self) -> [u8; 9] {
        concat_arrays!(self.to_bytes(), [ObjectFieldKeyId::Contents as u8])
    }

    fn key_contents(self, other: ObjectId) -> [u8; 17] {
        concat_arrays!(self.key_contents_prefix(), other.to_bytes())
    }

    fn key_containers_prefix(self) -> [u8; 9] {
        concat_arrays!(self.to_bytes(), [ObjectFieldKeyId::Container as u8])
    }

    fn key_containers(self, other: ObjectId) -> [u8; 17] {
        concat_arrays!(self.key_containers_prefix(), other.to_bytes())
    }

    /// Create an object
    ///
    /// Usually shouldnt be used directly. Use [`create_object_in`] instead
    pub async fn create_object(
        cf: &db::ColumnFamily,
        trans: &db::Transaction<'_>,
        name: &str,
    ) -> Result<ObjectId, DBError> {
        let obj = ObjectId::new_id().await?;
        trans.put_cf(cf, obj.key_name(), name)?;
        Ok(obj)
    }

    pub async fn create_object_in(
        cf: &db::ColumnFamily,
        trans: &db::Transaction<'_>,
        name: &str,
        container: ObjectId,
    ) -> Result<ObjectId, DBError> {
        let new_object = ObjectId::create_object(cf, trans, name).await?;
        ObjectId::place_object(cf, trans, container, new_object)?;
        Ok(new_object)
    }

    /// Check containment relation
    pub fn is_object_in(
        cf: &db::ColumnFamily,
        trans: &db::Transaction<'_>,
        object: ObjectId,
        container: ObjectId,
    ) -> Result<bool, DBError> {
        trans
            .get_cf(cf, container.key_contents(object))
            .map(|o| o.is_some())
            .map_err(DBError::DB)
    }

    /// Put an object into a container
    pub fn place_object(
        cf: &db::ColumnFamily,
        trans: &db::Transaction<'_>,
        container: ObjectId,
        object: ObjectId,
    ) -> Result<(), DBError> {
        trans.put_cf(cf, container.key_contents(object), &[])?;
        trans.put_cf(cf, object.key_containers(container), &[])?;

        Ok(())
    }

    /// Remove an object from a container
    ///
    /// Returns false if object was not in the container.
    /// Returns true if the object was in the container and was removed.
    pub fn remove_object(
        cf: &db::ColumnFamily,
        trans: &db::Transaction<'_>,
        container: ObjectId,
        content: ObjectId,
    ) -> Result<(), DBError> {
        trans.delete_cf(cf, container.key_contents(content))?;
        trans.delete_cf(cf, content.key_containers(container))?;
        content.drop_check(cf, trans)?;
        Ok(())
    }

    /// Free an object if it is not contained anywhere
    ///
    /// Return value is if the object was freed
    pub fn drop_check(
        self,
        cf: &db::ColumnFamily,
        trans: &db::Transaction<'_>,
    ) -> Result<bool, DBError> {
        // Writers are readers too.
        let has_container = trans
            .prefix_iterator_cf(cf, self.key_containers_prefix())
            .next()
            .transpose()?
            .is_some();

        if !has_container {
            // delete that orphan!

            // Get the orphan's contents
            for res in trans.prefix_iterator_cf(cf, self.key_contents_prefix()) {
                let (key, _) = res?;
                let content_id_bytes =
                    <[u8; 8]>::try_from(&key[9..17]).expect("staticly sized slice");
                let content_id = u64::from_be_bytes(content_id_bytes);
                let content =
                    ObjectId::from_raw(content_id).expect("idk what to do if this isnt a valid id");

                ObjectId::remove_object(cf, trans, self, content)?;
            }

            trans.delete_cf(cf, self.key_name())?;

            // TODO: delete the properties

            Ok(true)
        } else {
            Ok(false)
        }
    }
}
