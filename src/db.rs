use rocksdb::SingleThreaded;
use thiserror::Error;

pub use rocksdb::Error;
pub use rocksdb::TransactionDB;

pub type DB = rocksdb::TransactionDB<SingleThreaded>;
pub type Transaction<'db> = rocksdb::Transaction<'db, DB>;

pub use rocksdb::ColumnFamily;

fn format_bytes(bytes: &impl AsRef<[u8]>) -> String {
    use std::fmt::Write as _;

    let mut o = String::with_capacity(bytes.as_ref().len() * 2);

    for byte in bytes.as_ref() {
        write!(&mut o, "{byte:02x}").unwrap();
    }

    o
}

pub(crate) fn dump_db(db: &TransactionDB, name: &'static str) {
    let cf = db.cf_handle(name).expect("unable to get cf");

    println!("CF: {name}");
    for res in db.full_iterator_cf(&cf, rocksdb::IteratorMode::Start) {
        match res {
            Ok((k, v)) => {
                println!(
                    "{}: {} ({} bytes)",
                    format_bytes(&k),
                    format_bytes(&v),
                    v.len()
                );
            }
            Err(e) => {
                println!("Err: {e}");
            }
        }
    }
}

pub fn create_db(path: &std::path::Path) -> Result<DB, Error> {
    let cf_opts = rocksdb::Options::default();
    let object_cf = rocksdb::ColumnFamilyDescriptor::new("objects", cf_opts);

    let mut db_opts = rocksdb::Options::default();
    db_opts.create_missing_column_families(true);
    db_opts.create_if_missing(true);
    // db_opts.set_compression_type(rocksdb::DBCompressionType::Lz4);
    // db_opts.set_bottommost_compression_type(rocksdb::DBCompressionType::Zstd);

    let transaction_opts = rocksdb::TransactionDBOptions::default();

    rocksdb::TransactionDB::open_cf_descriptors(&db_opts, &transaction_opts, path, vec![object_cf])
}
